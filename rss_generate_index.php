<?php

require_once('magpierss/rss_fetch.inc'); // RSS library to fetch RSS news
require_once('incl/html_tmpl.inc'); // HTML Template
require_once('incl/rss_src.inc'); // RSS Sources


$rssTopic = array_keys($rss_links);
$topicCount = 0;

$limit = 8; // Notice limit per RSS

foreach ($rss_links as $topic) {

    $html = $header . $pageTitleOpn . $rssTopic[$topicCount] . $pageTitleClo . $row;

    $rssCount = 0;
    $count = 0;
    $rowCount = 0;

    $rssCat = array_keys($topic);

    foreach ($topic as $url) {

        if ($rowCount <> 0 && $rowCount % 3 === 0) {
            $html .= $row;
        }

        $rss = fetch_rss($url);

        if ($count == 0) {
            $html .= $columnOpn;
        }

        $html .= $linkUrlOpn . $rss->channel['link'] . $linkUrlClo . $titleOpn . $rssCat[$rssCount] . $titleClo . $linkUrlEnd;
        $rssCount++;
        $c = 0;

        foreach ($rss->items as $item) {
            $html .= $itemOpn . $linkUrlOpn . $item['link'] . $linkUrlClo . $item['title'] . $count . $linkUrlEnd . $itemClo;
            $count++;
            $c++;

            if ($limit && $limit == $c) {
                $count = 0; $rowCount++;
                $html .= $columnClo;
                continue(2);
            }
        }
    }

    $html .= $footer;

    if (!is_dir(strtolower($rssTopic[$topicCount]))) {
        // dir doesn't exist, make it and copy bootstrap.css file
      mkdir(strtolower($rssTopic[$topicCount]));
  }

  file_put_contents(strtolower($rssTopic[$topicCount]) . '/index.html', $html);

  $topicCount++;

}

?>
