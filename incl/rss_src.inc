<?php

$rss_links = array(
	'News' => array(
		'NYT World' => 'http://rss.nytimes.com/services/xml/rss/nyt/World.xml',
		'NYT US' => 'http://rss.nytimes.com/services/xml/rss/nyt/US.xml',
		'NYT Business' => 'http://rss.nytimes.com/services/xml/rss/nyt/Business.xml',
		'NYT Technology' => 'http://rss.nytimes.com/services/xml/rss/nyt/Technology.xml'
		),
	'Sports' => array(
		'NYT Sports' => 'http://rss.nytimes.com/services/xml/rss/nyt/Sports.xml',
		'Deadspin' => 'http://feeds.gawker.com/deadspin/full'
		)
	);

?>